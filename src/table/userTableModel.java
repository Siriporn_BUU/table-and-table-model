/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author informatics
 */
public class userTableModel extends AbstractTableModel {
    String[] columnNames = {"Username","Firstname","Lastname"};
    ArrayList<User> userList = Data.userList ;
    public userTableModel() {
        
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    

    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length ;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = userList.get(rowIndex) ;
        if(user == null) return "" ;
        switch(columnIndex) {
            case 0 : return user.getUsername();
            case 1 : return user.getFirstname();
            case 2 : return user.getLastname();
        }
        return "";
    }
    
}
