/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

/**
 *
 * @author informatics
 */
public class User {
    String Firstname ;
    String Lastname ;
    String Username ;
    String password ;
    float Weight ;
    float Height ;

    public User() {
    }
    
    public User(String Username,String Firstname, String Lastname,  String password, float Weight, float Height) {
        this.Firstname = Firstname;
        this.Lastname = Lastname;
        this.Username = Username;
        this.password = password;
        this.Weight = Weight;
        this.Height = Height;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String Firstname) {
        this.Firstname = Firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String Lastname) {
        this.Lastname = Lastname;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public float getWeight() {
        return Weight;
    }

    public void setWeight(float Weight) {
        this.Weight = Weight;
    }

    public float getHeight() {
        return Height;
    }

    public void setHeight(float Height) {
        this.Height = Height;
    }
    
}
